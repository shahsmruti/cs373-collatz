#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List
from cache import meta_cache

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>

    assert i, j > 0
    assert i, j < 1000000

    min_value = i if i < j else j
    max_value = i if i > j else j

    lazy_cache = [0] * 1000000
    # max-length to be return
    max_length = 1

    # iterate from min to max inclusively to find the max cycle length
    while min_value < max_value + 1:
        # check if there is a range for which we have the value stored in meta cache array
        if (
            min_value % 1000 == 1  # pylint: disable=C0330
            and (min_value + 999) < (max_value + 1)  # pylint: disable=C0330
            and (min_value + 999) % 1000 == 0  # pylint: disable=C0330
        ):
            # get the max cycle length value for that range from meta_cache
            # \
            # array and check if it is the new max cycle length
            meta_value = meta_cache[min_value // 1000]
            max_length = meta_value if meta_value > max_length else max_length
            # skip the values in that range
            min_value += 1000
        else:
            length = 1
            value = min_value
            # calculate the cycle length for the given value and update max_length if needed
            while value > 1:
                if (value % 2) == 0:
                    value = value // 2
                else:
                    value = (3 * value) + 1

                # check if we have already computed and stored the cycle length
                if (value < 1000000) and lazy_cache[value] != 0:
                    length += lazy_cache[value]
                    break
                else:
                    length += 1
            lazy_cache[min_value] = length
            min_value += 1
            assert length > 0
            max_length = length if length > max_length else max_length

    return max_length


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
